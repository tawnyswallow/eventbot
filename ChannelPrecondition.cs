﻿using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace EventBot
{
  public class ChannelPrecondition : PreconditionAttribute
  {
    public string Channel { get; set; }

#pragma warning disable 1998
    public override async Task<PreconditionResult> CheckPermissions(ICommandContext context, CommandInfo command, IServiceProvider services)
#pragma warning restore 1998
    {
      if(context.Channel.Name.Equals(Channel, StringComparison.InvariantCultureIgnoreCase))
        return PreconditionResult.FromSuccess();

      return PreconditionResult.FromError($"This command may only be performed in the `{Channel}` channel.");
    }
  }
}
