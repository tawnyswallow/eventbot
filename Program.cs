﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using EventBot.Models;
using EventBot.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EventBot
{
  public class Program
  {
    public static void Main() => new Program().MainAsync().GetAwaiter().GetResult();

    private readonly DiscordSocketClient _client;
    private readonly CommandService _commands = new CommandService();
    private readonly EnvVars _environmentVariables = new EnvVars();
    private readonly IServiceProvider _services;

    private Program()
    {
      _client = new DiscordSocketClient(new DiscordSocketConfig
      {
        LogLevel = LogSeverity.Info,
        MessageCacheSize = 64
      });

      _services = new ServiceCollection()
        .AddSingleton(_client)
        .AddSingleton(_commands)
        .AddSingleton<EventDbService>()
        .AddSingleton<NotificationService>()
        .AddEntityFrameworkNpgsql().AddDbContext<EventDbContext>(opts => opts.UseNpgsql(_environmentVariables.ConnectionString))
        .BuildServiceProvider();

      _client.Log += Log;
      _commands.Log += Log;

      _client.Ready += OnClientReady;
    }

    private async Task InstallCommands()
    {
      _client.MessageReceived += async (socketMessage) =>
      {
        if(!(socketMessage is SocketUserMessage msg))
          return;

        var argPos = 0;

        if(!(msg.HasCharPrefix('!', ref argPos) || msg.HasMentionPrefix(_client.CurrentUser, ref argPos)))
          return;

        var ctx = new SocketCommandContext(_client, msg);
        var result = await _commands.ExecuteAsync(ctx, argPos, _services);

        if(!result.IsSuccess && result.Error != CommandError.UnknownCommand)
        {
          await ctx.Channel.SendMessageAsync(result.ErrorReason);
        }
      };

      await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
    }

    public async Task MainAsync()
    {
      await InstallCommands();
      await _services.GetService<EventDbContext>().Database.EnsureCreatedAsync();

      await _client.LoginAsync(TokenType.Bot, _environmentVariables.Token);
      await _client.StartAsync();

      await Task.Delay(-1);
    }

    private Task OnClientReady()
    {
      _client.LatencyUpdated += async (i, i1) => { await _services.GetService<NotificationService>().Run(_environmentVariables.EventChannelId); };

      return Task.CompletedTask;
    }

    private Task Log(LogMessage msg)
    {
      switch(msg.Severity)
      {
        case LogSeverity.Critical:
        case LogSeverity.Error:
          Console.ForegroundColor = ConsoleColor.Red;
          break;
        case LogSeverity.Warning:
          Console.ForegroundColor = ConsoleColor.Yellow;
          break;
        case LogSeverity.Info:
          Console.ForegroundColor = ConsoleColor.White;
          break;
        default:
          Console.ForegroundColor = ConsoleColor.DarkGray;
          break;
      }

      Console.WriteLine($"{DateTime.Now,-19} [{msg.Severity,8}] {msg.Source}: {msg.Message} {msg.Exception}");
      Console.ResetColor();

      return Task.CompletedTask;
    }

    private class EnvVars
    {
      public string Token { get; }
      public ulong EventChannelId { get; }

      internal string ConnectionString { get; }

      public EnvVars()
      {
        var vars = Environment.GetEnvironmentVariables();

        Token = (string)vars["BOT_TOKEN"];
        ConnectionString = ParseConnectionString((string)vars["CONNECTION_STRING"]);
        EventChannelId = ulong.Parse((string)vars["EVENT_CHANNEL_ID"]);
      }

      private static string ParseConnectionString(string input)
      {
        if(!input.StartsWith("postgres"))
          return input;

        var uri = new Uri(input);
        var db = uri.AbsolutePath.Trim('/');
        var user = uri.UserInfo.Split(':')[0];
        var passwd = uri.UserInfo.Split(':')[1];
        var port = uri.Port > 0 ? uri.Port : 5432;

        return $"Server={uri.Host};Database={db};User Id={user};Password={passwd};Port={port}";
      }
    }
  }
}
