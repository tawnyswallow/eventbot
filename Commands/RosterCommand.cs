using System;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using EventBot.Services;

namespace EventBot.Commands
{
  [Group("event")]
  public class RosterCommand : ModuleBase<SocketCommandContext>
  {
    private readonly EventDbService _dbService;

    public RosterCommand(EventDbService dbService)
    {
      _dbService = dbService;
    }

    [Command("roster")]
    [Summary("`roster <EVENT_NUMBER>`\t Print the signup roster for the specified event")]
    [Remarks("To find `EVENT_NUMBER`, use `!event list`")]
    public async Task HandleRoster(int eventIdx, int offset = 0)
    {
      var events = await _dbService.ListEvents();

      if(events.Count < eventIdx)
      {
        await ReplyAsync("That event could not be found. Please try `!event list` to find the event number.");
      }

      var evt = events[eventIdx - 1];

      if(evt.Signups == null || evt.Signups?.Count == 0)
      {
        await ReplyAsync("No one has signed up for that event.");
      }
      else
      {
        var dtOffset = ((DateTimeOffset)DateTime.SpecifyKind(evt.Occurs, DateTimeKind.Utc)).ToOffset(TimeSpan.FromHours(offset));
        var sb = new StringBuilder($"**{evt.Name} - {dtOffset:f} (GMT {dtOffset.Offset.Hours:+0;-#})**");

        sb.AppendLine();
        sb.AppendLine(evt.GetRoster(Context.Client));

        await ReplyAsync(sb.ToString());
      }
    }
  }
}
