using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using EventBot.Models;
using EventBot.Services;

namespace EventBot.Commands
{
  [Group("event")]
  public class DeleteCommand : ModuleBase<SocketCommandContext>
  {
    private readonly EventDbService _dbService;

    public DeleteCommand(EventDbService dbService)
    {
      _dbService = dbService;
    }

    [Command("delete")]
    [Summary("`delete <EVENT_NUMBER>`\t Delete the given event")]
    [Remarks("To find `EVENT_NUMBER`, use `!event list`")]
    public async Task HandleDelete(int eventIdx)
    {
      if(eventIdx == -1 && ((SocketGuildUser)Context.User).GuildPermissions.Administrator)
      {
        await _dbService.TruncateEvents();
        await Context.Message.AddReactionAsync(ReactionEmoji.ThumbsUp);
        return;
      }

      var events = await _dbService.ListEvents();

      if(events.Count < eventIdx)
      {
        await ReplyAsync("That event could not be found. Please try `!event list` to find the event number.");
      }
      else
      {
        var evt = events[eventIdx - 1];

        if(evt.CreatedBy.Discriminator.Equals(Context.User.Discriminator) || ((SocketGuildUser)Context.User).GuildPermissions.ManageMessages)
        {
          await _dbService.DeleteEvent(events[eventIdx - 1]);
          await Context.Message.AddReactionAsync(ReactionEmoji.ThumbsUp);
        }
        else
        {
          await ReplyAsync("You do not have permission to modify that event.");
        }
      }
    }
  }
}
