using System.Threading.Tasks;
using Discord.Commands;
using EventBot.Models;
using EventBot.Services;

namespace EventBot.Commands
{
  [Group("event")]
  public class UnSignupCommand : ModuleBase<SocketCommandContext>
  {
    private readonly EventDbService _dbService;

    public UnSignupCommand(EventDbService dbService)
    {
      _dbService = dbService;
    }

    [Command("unsignup")]
    [Alias("remove")]
    [Summary("`unsignup <EVENT_NUMBER>`\t Remove your signup from the specified event")]
    [Remarks("To find `EVENT_NUMBER`, use `!event list`")]
    public async Task HandleUnSignup(int eventIdx)
    {
      var events = await _dbService.ListEvents();

      if(events.Count < eventIdx)
      {
        await ReplyAsync("That event could not be found. Please try `!event list` to find the event number.");
      }
      else
      {
        var evt = events[eventIdx - 1];

        await _dbService.RemoveSignup(evt, new DiscordUser
        {
          Discriminator = Context.User.Discriminator,
          Username = Context.User.Username
        });
        await Context.Message.AddReactionAsync(ReactionEmoji.ThumbsUp);
      }
    }
  }
}
