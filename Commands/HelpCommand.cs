﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;

namespace EventBot.Commands
{
  [Group("event")]
  public class HelpCommand : ModuleBase<SocketCommandContext>
  {
    private readonly CommandService _commandService;

    public HelpCommand(CommandService commandService)
    {
      _commandService = commandService;
    }

    [Command("help")]
    [Summary("`help`\t Print this message")]
    [Remarks("`help [OPTION]`\t Print usage and examples for working with event commands.")]
    public async Task SendHelp([Remainder] string command = "")
    {
      var cmds = (from c in _commandService.Commands
                  select c);
      var sb = new StringBuilder();

      if(string.IsNullOrEmpty(command))
      {
        sb.AppendLine("Usage: `!event [OPTION]`");
        sb.AppendLine();
        sb.AppendLine("Create or signup for a guild event");
        sb.AppendLine("Available options:");

        foreach(var cmd in cmds.Where(c => !string.IsNullOrEmpty(c.Summary)))
        {
          sb.AppendLine(cmd.Summary);
        }

        sb.AppendLine().AppendLine("For additional information on any of the options above, try `!event help [OPTION]`");
      }
      else
      {
        var cmd = (from c in cmds
                   where c.Name.Equals(command, StringComparison.InvariantCultureIgnoreCase)
                   select c).FirstOrDefault();

        if(cmd == null)
        {
          sb.AppendLine("That command is not supported.");
        }
        else
        {
          sb.AppendLine(cmd.Summary);
          sb.AppendLine();
          sb.AppendLine(cmd.Remarks);
        }
      }

      await ReplyAsync(sb.ToString());
    }
  }
}
