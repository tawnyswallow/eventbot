using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using EventBot.Models;
using EventBot.Services;

namespace EventBot.Commands
{
  [Group("event")]
  public class SignupCommand : ModuleBase<SocketCommandContext>
  {
    private readonly EventDbService _dbService;

    public SignupCommand(EventDbService dbService)
    {
      _dbService = dbService;
    }

    [Command("signup")]
    [Summary("`signup <EVENT_NUMBER> <ROLE>`\t Signup for the specified event")]
    [Remarks("To find `EVENT_NUMBER`, use `!event list`" +
             "\r\rOptions you can use for `ROLE`:" +
             "\r* `Tank` or `T`" +
             "\r* `Healer` or `H`" +
             "\r* `mDD` or `mDPS` or `melee` (**m**elee damage dealer)" +
             "\r* `rDD` or `rDPS` or `ranged` (**r**anged damage dealer)" +
             "\r\rYou can signup with multiple role options with `/`, such as `event signup 1 H/mDD`.")]
    public async Task HandleSignup(int eventIdx, string roleInput = "")
    {
      if(Context.User is SocketGuildUser user && !user.Roles.Any(role => role.Name.Equals("Forlorn Hope")))
      {
        await ReplyAsync("You must join the Forlorn Hope before signing up for an event!");
        return;
      }

      var events = await _dbService.ListEvents();

      if(events.Count < eventIdx)
      {
        await ReplyAsync("That event could not be found. Please try `!event list` to find the event number.");
      }
      else if(string.IsNullOrEmpty(roleInput))
      {
        await ReplyAsync("Please specify a role. For a list of role options, try `!event help signup`.");
      }
      else
      {
        var evt = events[eventIdx - 1];
        var existingSignup = evt.Signups.FirstOrDefault(s => s.User.Discriminator.Equals(Context.User.Discriminator));
        var role = ParseRoleInput(roleInput);

        if(Role.Unknown.Equals(role))
        {
          await ReplyAsync("That role could not be understood. Please see `!event help signup` for available role options.");
        }
        else if(existingSignup != null && existingSignup.Role.Equals(role))
        {
          await ReplyAsync("You have already signed up for this event!");
        }
        else
        {
          var signup = new EventSignup
          {
            User = new DiscordUser
            {
              Discriminator = Context.User.Discriminator,
              Username = Context.User.Username
            },
            Role = role
          };

          if(existingSignup != null)
          {
            await _dbService.UpdateSignup(evt, signup);
          }
          else
          {
            await _dbService.AddSignup(evt, signup);
          }

          await Context.Message.AddReactionAsync(ReactionEmoji.ThumbsUp);
        }
      }
    }

    private Role ParseRoleInput(string input)
    {
      var role = Role.Unknown;

      if(input.Contains('/'))
      {
        var parts = input.Split('/');
        role = parts.Aggregate(role, (current, part) => current | ParseRoleInput(part));
      }
      else
      {
        switch(input.ToLower())
        {
          case "tank":
          case "t":
            role = Role.Tank;
            break;
          case "healer":
          case "h":
            role = Role.Healer;
            break;
          case "mdd":
          case "mdps":
          case "melee":
            role = Role.MeleeDamageDealer;
            break;
          case "rdd":
          case "rdps":
          case "ranged":
            role = Role.RangedDamageDealer;
            break;
          default:
            role = Role.Unknown;
            break;
        }
      }

      return role;
    }
  }
}
