using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord.Commands;
using EventBot.Models;
using EventBot.Services;

namespace EventBot.Commands
{
  [Group("event")]
  public class CreateCommand : ModuleBase<SocketCommandContext>
  {
    private readonly EventDbService _dbService;

    public CreateCommand(EventDbService dbService)
    {
      _dbService = dbService;
    }

    [Command("create")]
    [Summary("`create \"<DATE> <TIME>\" <ROLE_SLOTS> <NAME>`\t Create a named event")]
    [Remarks("Fill `ROLE_SLOTS` with an alphanumeric indicator of how many tanks (T), healers (H), and damage dealers (DD) that you need." +
             "\r\rExamples (_make sure to put quotes (\") around the date and time_):" +
             "\r   `!event create \"1/20/18 19:00\" 2T2H8DD example`" +
             "\r   `!event create \"1/20/18 15:00-4\" 2T2H8DD example`\t(indicating timezone offset of -4 from UTC)" +
             "\r   `!event create \"2018-01-20 19:00\" 2T2H8DD example`" +
             "\r   `!event create \"2018-01-20 7 PM\" 2T2H8DD example`" +
             "\r   `!event create 2018-01-20T15:00-4 2T2H8DD example`" +
             "\r\rAll of the above examples create an event named _example_ on January 20th at 7:00 PM UTC (GMT-0), requiring two tanks, two healers, and eight damage dealers.")]
    public async Task HandleCreate(string dateTimeInput, string roleSlots, [Remainder] string name = "")
    {
      if(!DateTime.TryParse(dateTimeInput, out var date))
      {
        await ReplyAsync($"The date `{dateTimeInput}` could not be understood. See `!event help create` for formatting examples.");
        return;
      }

      if(string.IsNullOrEmpty(name))
      {
        await ReplyAsync("An event name is required.");
      }
      else if(!Event.RoleSlotPattern.IsMatch(roleSlots))
      {
        await ReplyAsync("The slot format could not be understood. Please see `!event help create` for formatting options.");
      }
      else
      {
        var evt = new Event
        {
          Name = name,
          Occurs = date.ToUniversalTime(),
          RoleSlots = roleSlots,
          Signups = new List<EventSignup>(),
          CreatedBy = new DiscordUser
          {
            Discriminator = Context.User.Discriminator,
            Username = Context.User.Username
          }
        };

        await _dbService.AddEvent(evt);
        await Context.Message.AddReactionAsync(ReactionEmoji.ThumbsUp);
      }
    }
  }
}
