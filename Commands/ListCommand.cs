using System;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using EventBot.Services;

namespace EventBot.Commands
{
  [Group("event")]
  public class ListCommand : ModuleBase<SocketCommandContext>
  {
    private readonly EventDbService _dbService;

    public ListCommand(EventDbService dbService)
    {
      _dbService = dbService;
    }

    [Command("list")]
    [Alias("ls")]
    [Summary("`list`\t List all active events")]
    [Remarks("You can list events with a specific time offset, as well:" +
             "\r   `!event list -5`\t displays event times in Eastern Standard Time" +
             "\r   `!event list -7`\t displays event times in Pacific Daylight Time" +
             "\r   `!event list 2`\t displays event times in Eastern Europe Standard Time")]
    public async Task HandleList(int offset = 0)
    {
      var events = await _dbService.ListEvents();

      if(events.Count == 0)
      {
        await ReplyAsync("There are currently no scheduled events.");
        return;
      }

      var sb = new StringBuilder();
      var idx = 1;

      foreach(var evt in events)
      {
        var dtOffset = ((DateTimeOffset)DateTime.SpecifyKind(evt.Occurs, DateTimeKind.Utc)).ToOffset(TimeSpan.FromHours(offset));

        sb.AppendLine($"{idx}: _{evt.Name}_ ({evt.RoleSlots.ToUpper()}) occurs {dtOffset:f} (GMT {dtOffset.Offset.Hours:+0;-#}): currently {evt.Signups?.Count ?? 0} signed up");
        idx++;
      }

      await ReplyAsync(sb.ToString());
    }
  }
}
