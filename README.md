# EventBot
a Discord event management bot!

## Introduction
This project is for a Discord bot designed to manage events with signup sheets and channel notifications.

Technologies used:

* C# and .NET Core 2.x
* [Discord.NET](https://github.com/RogueException/Discord.Net)
* [Pivotal.io](https://pivotal.io)

## Deployment model
The bot is currently hosted on a Pivotal droplet.

### Environment variables
The bot requires the following environment variables:

* `BOT_TOKEN` - Discord OAuth token
* `CONNECTION_STRING` - A PostgreSQL connection string for event storage, in the form of `Server=xyz;Database=xyz;Username=xyz;Password=xyz`
* `EVENT_CHANNEL_ID` - ID of the Discord channel to post event notifications to

To find the `ID` values, enable Discord developer mode (under Settings > Appearance) and right-click a guild or channel to find the `Copy ID` command.

## Compiling
Compile a stand-alone executable targeting the Pivotal Ubuntu 14 x64 platform, using

    dotnet publish -c Release -r ubuntu.14.04-x64

## Deploying
Pivotal uses the associated `buildpack.yml` and `manifest.yml` files to determine build configurations, including the aforementioned environment variables.

Run the `publish` command above, then use the Pivotal CloudFormation CLI to push:

    cf push -p <PATH_TO_PUBLISH_OUTPUT>
    
The `PATH_TO_PUBLISH_OUTPUT` should be the actual `publish` directory, such as `bin\Release\netcoreapp2.0\ubuntu.14.04-x64\publish`
