using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;

namespace EventBot.Models
{
  public class Event
  {
    public static readonly Regex RoleSlotPattern = new Regex(@"^(\d(?:t|h|dd))(\d(?:t|h|dd))?(\d(?:t|h|dd))?$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

    public Guid Id { get; set; }

    public DiscordUser CreatedBy { get; set; }
    public string Name { get; set; }
    public DateTime Occurs { get; set; }
    public NotificationState NotificationState { get; set; }
    public string RoleSlots { get; set; }
    public List<EventSignup> Signups { get; set; }

    public string GetRoster(IDiscordClient client)
    {
      var sb = new StringBuilder();
      var signups = Signups.OrderBy(s => s.SignupTime).ToList();

      Task<IUser> getUser(DiscordUser u)
      {
        return client.GetUserAsync(u.Username, u.Discriminator);
      }

      var tanks = (from s in signups where s.Role.HasFlag(Role.Tank) && getUser(s.User) != null select getUser(s.User)).ToList();
      var healers = (from s in signups where s.Role.HasFlag(Role.Healer) && getUser(s.User) != null select getUser(s.User)).ToList();
      var damagers = (from s in signups
                      where s.Role.HasFlag(Role.MeleeDamageDealer) || s.Role.HasFlag(Role.RangedDamageDealer) && getUser(s.User) != null
                      select new
                      {
                        s.Role,
                        User = getUser(s.User)
                      }).ToList();

      if(tanks.Any())
      {
        sb.AppendLine("Tanks");
        tanks.ForEach(u => sb.AppendLine($"- {u.Result.Mention}"));
      }

      if(healers.Any())
      {
        sb.AppendLine("Healers");
        healers.ForEach(u => sb.AppendLine($"- {u.Result.Mention}"));
      }

      if(damagers.Any())
      {
        sb.AppendLine("Damage dealers");
        damagers.ForEach(s => sb.AppendLine($"- {s.User.Result.Mention} ({GetDDRoles(s.Role)})"));
      }

      return sb.ToString();
    }

    private static string GetDDRoles(Role role)
    {
      if(role.HasFlag(Role.MeleeDamageDealer) && role.HasFlag(Role.RangedDamageDealer))
        return "melee or ranged";

      if(role.HasFlag(Role.MeleeDamageDealer))
        return "melee";

      if(role.HasFlag(Role.RangedDamageDealer))
        return "ranged";

      throw new ArgumentOutOfRangeException(nameof(role), role, null);
    }
  }
}
