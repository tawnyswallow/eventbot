using Discord;

namespace EventBot.Models
{
  public static class ReactionEmoji
  {
    public static readonly IEmote ThumbsUp = new Emoji("👍");
  }
}
