using System;

namespace EventBot.Models
{
  public class EventSignup
  {
    public Guid Id { get; set; }

    public Role Role { get; set; }
    public DateTime SignupTime { get; set; } = DateTime.UtcNow;
    public DiscordUser User { get; set; }
  }
}
