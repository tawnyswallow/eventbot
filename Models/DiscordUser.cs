using System;

namespace EventBot.Models
{
  public class DiscordUser
  {
    public Guid Id { get; set; }

    public string Discriminator { get; set; }
    public string Username { get; set; }
  }
}
