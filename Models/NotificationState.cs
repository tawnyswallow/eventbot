using System;

namespace EventBot.Models
{
  [Flags]
  public enum NotificationState : byte
  {
    Pending = 0x00,
    Notified24h = 0x01,
    Notified1h = 0x02
  }
}
