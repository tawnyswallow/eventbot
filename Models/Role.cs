using System;

namespace EventBot.Models
{
  [Flags]
  public enum Role : byte
  {
    Unknown = 0,
    Tank = 0x01,
    Healer = 0x02,
    MeleeDamageDealer = 0x04,
    RangedDamageDealer = 0x08
  }
}
