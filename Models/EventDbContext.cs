using Microsoft.EntityFrameworkCore;

namespace EventBot.Models
{
  public class EventDbContext : DbContext
  {
    public DbSet<DiscordUser> DiscordUsers { get; set; }
    public DbSet<Event> Events { get; set; }

    public EventDbContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      builder.Entity<EventSignup>()
        .HasOne<Event>()
        .WithMany(s => s.Signups)
        .OnDelete(DeleteBehavior.Cascade);
    }
  }
}
