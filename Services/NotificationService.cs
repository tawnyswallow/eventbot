using System;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using EventBot.Models;

namespace EventBot.Services
{
  public class NotificationService
  {
    private readonly DiscordSocketClient _client;
    private readonly EventDbService _dbService;

    public NotificationService(DiscordSocketClient client, EventDbService dbService)
    {
      _client = client;
      _dbService = dbService;
    }

    public Task Run(ulong eventChannelId)
    {
      Task.Run(async () =>
      {
        var events = await _dbService.ListEvents();

        foreach(var evt in events)
        {
          if(evt.Signups?.Count == 0)
            return;

          if(evt.Occurs > DateTime.UtcNow.AddDays(1))
            return;

          if(!evt.NotificationState.HasFlag(NotificationState.Notified24h))
          {
            await SendRoster(evt, "day", eventChannelId);
            await _dbService.AddNotificationState(evt, NotificationState.Notified24h);
          }
          else if(!evt.NotificationState.HasFlag(NotificationState.Notified1h) && evt.Occurs <= DateTime.UtcNow.AddHours(1))
          {
            await SendRoster(evt, "hour", eventChannelId);
            await _dbService.AddNotificationState(evt, NotificationState.Notified1h);
          }

          await Task.Delay(100); // avoid rate limit
        }
      });

      return Task.CompletedTask;
    }

    private async Task SendRoster(Event evt, string period, ulong channelId)
    {
      if(_client.GetChannel(channelId) is SocketTextChannel channel)
      {
        var sb = new StringBuilder($"**{evt.Name} - Starting in less than one {period}: {evt.Occurs:t} (GMT +0)**");

        sb.AppendLine();
        sb.AppendLine(evt.GetRoster(_client));

        await channel.SendMessageAsync(sb.ToString());
      }
    }
  }
}
