using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventBot.Models;
using Microsoft.EntityFrameworkCore;

namespace EventBot.Services
{
  public class EventDbService : IDisposable
  {
    private readonly EventDbContext _context;

    public EventDbService(EventDbContext context)
    {
      _context = context;
    }

    public async Task AddEvent(Event evt)
    {
      _context.Events.Add(evt);
      await _context.SaveChangesAsync();
    }

    public async Task AddNotificationState(Event evt, NotificationState state)
    {
      evt.NotificationState |= state;
      await _context.SaveChangesAsync();
    }

    public async Task AddSignup(Event evt, EventSignup signup)
    {
      if(evt.Signups == null)
      {
        evt.Signups = new List<EventSignup>();
      }

      evt.Signups.Add(signup);
      await _context.SaveChangesAsync();
    }

    public async Task DeleteEvent(Event evt)
    {
      _context.Events.Remove(evt);
      await _context.SaveChangesAsync();
    }

    public async Task<List<Event>> ListEvents()
    {
      var query = (from e in _context.Events
                   where e.Occurs >= DateTime.UtcNow
                   orderby e.Occurs
                   select e)
        .Include(e => e.Signups)
        .ThenInclude(s => s.User)
        .Include(e => e.CreatedBy);

      return await query.ToListAsync();
    }

    public async Task RemoveSignup(Event evt, DiscordUser user)
    {
      var listUser = evt.Signups?.FirstOrDefault(s => s.User.Discriminator.Equals(user.Discriminator));

      if(listUser != null)
      {
        evt.Signups.Remove(listUser);
        await _context.SaveChangesAsync();
      }
    }

    public async Task TruncateEvents()
    {
      _context.Events.RemoveRange(_context.Events);
      await _context.SaveChangesAsync();
    }

    public async Task UpdateSignup(Event evt, EventSignup signup)
    {
      if(evt.Signups == null)
      {
        evt.Signups = new List<EventSignup>();
      }

      evt.Signups.First(s => s.User.Discriminator.Equals(signup.User.Discriminator)).Role = signup.Role;
      await _context.SaveChangesAsync();
    }

    public void Dispose()
    {
      _context?.Dispose();
    }
  }
}
